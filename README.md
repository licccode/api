# API接口文档

## 接口分页
### 统一参数
| 参数名 | 参数类型 |是否必填 | 默认值 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :----: | :---- |
| page | int | 否 | 1 | 10 | 页码 |
| rows | string | 否 | 10 | 10 | 每页条数 |
| sortBy | string | 否 | id | 100 | 排序字段 |
| desc | Boolean | 否 | false | Boolean | 排序方式：desc=true按排序字段倒序排，不传按排序字段正序排 |

### 参考文件相对路径
app/api/controller/Page.php
### 返回参数
	{
		"total":24,
		"items":[
			{"id":29,"uv":0,"pv":0,"day":"2021-07-15","create_at":"2021-07-16 00:01:01"},
			{"id":28,"uv":0,"pv":0,"day":"2021-07-14","create_at":"2021-07-15 00:01:01"},
			{"id":27,"uv":1,"pv":29,"day":"2021-07-13","create_at":"2021-07-14 00:01:01"},
			{"id":26,"uv":1,"pv":873,"day":"2021-07-12","create_at":"2021-07-13 00:01:01"},
			{"id":25,"uv":1,"pv":171,"day":"2021-07-11","create_at":"2021-07-12 00:01:01"}
		],
		"totalPage":5
	}
### 附代码(分页示例)
	namespace app\api\controller;
	
	class Page extends ApiBaseController
	{
	    protected $table='AppDaypuvCollect';
		
		//分页例子
	    public function page(){
	        $db = $this->app->db->name($this->table);
	
	        $items = $db->page($this->_page,$this->_rows)
	            ->order($this->_sort,$this->_desc)
	            ->select();
	        //总记录数
	        $total  = $db->count();
	        //总页码
	        $totalPage = ceil($total/$this->_rows);
	        $data = [
	            'total' =>$total,
	            'items' => $items,
	            'totalPage' => $totalPage
	        ];
	        return json($data);
	    }
	}

## 一、图片上传同时做OCR识别接口
### 接口地址：https://www.zzjsxns.top/api/oss_upload/upload
### 请求方式：POST
### 请求参数 
| 参数名 | 是否必填 | 默认 | 解释 |
| :----:| :----:| :----: | :---- |
| image | 是 | image | 上传file的name  举例:\<input type="file" name="image" id="file"> |
| type | 否 | 0 | 0：只上传图片，1：识别身份证正面，2：识别身份证背面，3：识别驾照，4：人脸识别图片上传 |
| source | 否 | 0 | 0：管家端，1：客户端 |
| user_id | 客户端必传 | 0 | 用户ID，客户端必传 |

    
### 返回示例
#### tip:单纯上传图片返回示例  type=0
    {
    "errCode":0,
    "msg":"上传成功",
    "data":{"file_url":"https:\/\/speed.zzjsxns.top\/file\/48476fb3139cb0271746085059456d49\/14025520687.jpg","file_name":"14025520687.jpg"}
    }
    
#### tip:上传并识别身份证正面返回示例 type=1
    成功示例：
    {
    "errCode":0,
    "msg":"上传成功",
    "data":
        {
        "file_url":"https:\/\/speed.zzjsxns.top\/file\/3a02ccaae8f1ba97d1cdbf502543c338\/16535030917.jpg",
        "file_name":"16535030917.jpg"
        },
    "card_info":
        {
        "card_type":1,    //1：识别身份证正面，2：识别身份证背面，3：识别驾照，4：人脸识别
        "msg":"识别正常",
        "name":"李聪聪",
        "sex":"男",
        "nation":"汉",
        "address":"北京市昌平区回龙观龙博苑二区XXX",
        "idCard_no":"412826199004264212",
        "birthday":"19900426",
        "log_id":1412696353334395814,  //识别日志，前端用不到
        "err":0
        },
    "auth":
        {
        "pass":1,   //0：此图片未通过验证，1：此图片OCR识别出的信息验证通过。
        "msg":"验证通过", // 未通过时显示未通过原因，前端可直接将此信息反馈给用户。
        "all_pass":1  // 1：所有验证信息均已通过，可以下单，0：还未通过所有的验证
        }
    }
    
    失败示例：
    {
    "errCode":18,
    "msg":"上传成功",
    "data":[],
    "card_info":
        {
        "err":18,
        "msg":"Open api qps request limit reached"
        }
    }

#### tip:上传并识别身份证反面返回示例 type=2
    成功示例：
    {
    "errCode":0,
    "msg":"上传成功",
    "data":
        {
        "file_url":"https:\/\/speed.zzjsxns.top\/file\/e7ebf6489fed6cc8a3b64de16d8639ff\/16595589486.jpeg",
        "file_name":"16595589486.jpeg"
        },
    "card_info":
        {
        "card_type":2,
        "msg":"识别正常",
        "end_date":"20370324",      //身份证到期时间
        "sign_body":"平舆县公安局",   //签发地
        "sign_date":"20170324",     //签发日期
        "log_id":1412697887442644019,
        "err":0
        },
    "auth":
        {
        "pass":1,   // 0：此图片未通过验证，1：此图片OCR识别出的信息验证通过。
        "msg":"验证通过", // 未通过时显示未通过原因，前端可直接将此信息反馈给用户。
        "all_pass":1  // 1：所有验证信息均已通过，可以下单，0：还未通过所有的验证
        }
    }
    
    失败示例：
    {
    "errCode":18,
    "msg":"上传成功",
    "data":[],
    "card_info":{"err":18,"msg":"Open api qps request limit reached"}
    }
    
#### tip:上传驾照并识别驾照信息  type=3
    成功示例
    {
    "errCode":0,
    "msg":"上传成功",
    "data":{
        "file_url":"https:\/\/speed.zzjsxns.top\/file\/09e7071885d3a27f7a028ca74f704079\/17084543770.jpeg",
        "file_name":"17084543770.jpeg"
        },
    "card_info":{
        "err":0,
        "card_type":3,
        "log_id":1412700114581961571,
        "name":"李聪聪",   //驾驶人姓名
        "end_date":"20270622",  //驾照到期时间
        "card_no":"412826199004264212", //驾驶证号(同身份证号，要与身份证号比对，比对不通过提示提交人工审核)
        "start_date":"20110622",    //驾照首次发放时间，可用于算驾龄
        "driver_type":"C1"  //驾照类型
        },
    "auth":{
        "pass":0,
        "msg":"身份证号和驾驶证号不一致",
        "all_pass":0
        }
    }

    失败示例：
    {
    "errCode":18,
    "msg":"上传成功",
    "data":[],
    "card_info":{"err":18,"msg":"Open api qps request limit reached"}
    }

#### tip:人脸识别照片上传 type=4
    成功示例：
    {
		"errCode":0,
		"msg":"验证成功",
		"data":{
			"file_url":"https://speed.zzjsxns.top/file/83c3c30d57520b1d83c9a550819b1682/11552889191.jpeg",
			"file_name":"11552889191.jpeg"
		},
		"auth":{
			"pass":1,
			"msg":"验证成功",
			"all_pass":1
		}
	}
    失败示例：
    {
		"errCode":6,
		"msg":"No permission to access data",
		"data":{
			"file_url":"https://speed.zzjsxns.top/file/83c3c30d57520b1d83c9a550819b1682/11552889191.jpeg",
			"file_name":"11552889191.jpeg"
		},
		"auth":{
			"pass":0,
			"msg":"No permission to access data",
			"all_pass":0
		}
	}
  
---	
## 二、银行卡接口
### 1、添加/修改 银行卡
#### 接口地址 
https://www.zzjsxns.top/api/bank/updateBank
#### 请求方式：GET 或 POST
#### 请求参数：
| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| user_id | int | 是 | 10 | 用户ID |
| name | string | 是 | 10 | 账户名 |
| bank_name | string | 是 | 50 | 银行类别(银行名) |
| card_no | string | 是 | 40 | 银行卡号 |
| opening_bank | string | 是 | 40 | 开户行 |

#### 请求示例
https://www.zzjsxns.top/api/bank/updateBank?user_id=4&name=李聪聪&bank_name=工商银行&card_no=2343459485949545454&opening_bank=工商银行北京回龙观支行

#### 接口返回
成功返回：success
失败返回：null

### 2、查询用户银行卡信息
#### 接口地址
https://www.zzjsxns.top/api/bank/getBankInfo
#### 请求参数
| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| user_id | int | 是 | 10 | 用户ID |

#### 返回示例
    {
        "uid":4,
        "name":"李聪聪",
        "bank_name":"招商银行",
        "card_no":"2343459485949545454",
        "opening_bank":"北京回龙观支行",
        "create_at":"2021-07-08 17:25:20"
    }

---
## 三、合同相关接口
### 1、初始化合同
#### tip:何时调用及订单里合同状态流转说明：
*在客户支付完以后提示签署合同时调用或者在订单列表/订单详情页里面判断订单contract字段为‘0’时调用此接口，
此接口里返回的pageUrl(有效期30分钟)为合同签署页面，直接新开页面打开此URL就可以了。
用户请求以后合同会自动初始化并更改订单表contract字段为‘1’，往订单表contract_id字段插入合同ID。
用户全部签署完以后回调到我们服务器，这时会更改订单表里contract字段值为‘2’，
把合同查看/下载链接更新进订单表contract_url字段（合同是pdf文件）*
>**订单里contract字段值含义说明**
>*0:合同未初始化，1:合同已初始化还未签署完成，2：合同已完成签署*

#### 请求地址：https://www.zzjsxns.top/api/contracts/initcontract
#### 请求方式：POST
#### 请求参数：
| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| user_id | int | 是 | 10 | 用户ID |
| order_id | int | 是 | 10 | 订单ID |

#### 返回示例
##### *正确返回示例：*
	{
		"result":{
			"pageUrl":"https://cloudapi.qiyuesuo.com/contract/share/2850963669544686191?openPageToken=6cf8c3af-e4e1-4549-a3bb-93b8c7140930"
		},
		"code":0,  //code = 0 为正确，code > 0 为请求出错
		"message":"SUCCESS"
	}
##### *错误返回示例：*
	{
		"message":"DOCUMENT PARAM LACK,发起方未将必填参数填写完:账户名",
		"code":1116
	}

### 2、再次调起已初始化的合同
#### tip:何时调用：
*在订单列表页面或者订单详情页，如果订单contract字段值为‘1’则调用此方法，
并取出订单里contract_id字段值当成参数传入此接口（contract字段值为‘1’说明该合同已经初始化并未签署完成）*

#### 请求地址：
https://www.zzjsxns.top/api/contracts/view
#### 请求方式：POST
#### 请求参数：
| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| cid | int | 是 | 10 | 合同ID |

#### 返回示例
##### *正确返回示例：*
	{
		"result":{
			"pageUrl":"https://cloudapi.qiyuesuo.com/contract/share/2850963669544686191?openPageToken=6cf8c3af-e4e1-4549-a3bb-93b8c7140930"
		},
		"code":0,
		"message":"SUCCESS"
	}
##### *错误返回示例：*
	{
		"message":"DOCUMENT PARAM LACK,发起方未将必填参数填写完:账户名",
		"code":1116
	}

## 四、提交人工审核
### tip:何时调用
*当用户上传的信息自动审核不通过时*
### 请求地址：
https://www.zzjsxns.top/api/oss_upload/manpower
### 请求方法：POST
### 请求参数：
| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| user_id | int | 是 | 10 | 用户ID |
| remark | string | 否 | 100 | 备注 |

### 返回示例：
>成功返回 'success', 失败返回 null

## 五、出车视频
### tip:前端调用说明
*前端在首页5秒调用一次此接口，调用时先判断localStorge里有没有视频ID，如果有视频ID则带上视频ID，如果没有可不传。接口返回数据后前端需把id值作为视频ID存到localStorage里，下次请求时参数里需带上此视频ID(vid)*
### 请求地址：
https://www.zzjsxns.top/api/video/video
### 请求类型 GET/POST
### 请求参数：
| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| user_id | int | 是 | 10 | 用户ID |
| vid | int | 否 | 10 | 视频ID |

### 返回示例：
	有视频时返回：
	{
		"id":2, 	//视频ID
		"title":"玛莎拉蒂出车视频", 	//视频描述
		"url":"https://speed.zzjsxns.top/0f/56ff5ad16d8a6dab6dc1b858766655.mp4" //视频地址
	}
	无视频返回null
	
## 六、人脸识别获取ACCESS_TOKEN
### 请求地址
https://www.zzjsxns.top/api/token/getToken
### 请求参数：无
### 请求类型：POST
### 返回示例：
	成功示例：
	{
		"errCode":0,
		"token":"24.81ce8a14bd0c5daddd207c900d73df60.2592000.1628650137.282335-24271765",
		"msg":"success"
	}
	失败示例：
	{
		"errCode":1,
		"token":"",
		"msg":"获取ACCESS_TOKEN失败"
	}
	
## 七、获取卡证信息
### 请求地址：
https://www.zzjsxns.top/api/card/info
### 请求类型：GET/POST
### 请求参数：
| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| user_id | int | 是 | 10 | 用户ID |

### 返回示例：
	正确示例：
	{
		"code":1,
		"info":"success",
		"data":{
			"name":"李聪聪",		//身份证姓名
			"id_no":"412826198904264212",	//身份证号
			"id_card_front":"https:\/\/speed.zzjsxns.top\/file\/92c77dfc3182460019e552a4667a1b33\/16523736541.jpg",	//身份证正面
			"id_card_back":"https:\/\/speed.zzjsxns.top\/file\/b2ea48f22f33d30652eb8d978f05d616\/16340054013.jpeg",	//身份证反面
			"driving_img":"https:\/\/speed.zzjsxns.top\/file\/ab1cf147168c980042cea7e659544e2a\/15530080532.jpeg",	//驾照正面
			"user_auth":0,	//人脸识别是否通过 0：未通过，1：通过
			"reason":"No permission to access data",	//审核未通过原因
			"license_take_time":"20110622",	//驾照领证日期
			"license_end_time":"20270622",	//驾照到期日期
			"license_level":"C1",	//驾照类型
			"license_no":"412826199004264212",	//驾照号
			"license_name":"李聪聪",	//驾照姓名
			"status":2	//状态：0:未提交到人工审核 1:审核通过 2:待人工审核 3:人工审核未通过 4:自动审核未通过
		}
	}
	错误示例：
	{
		"code":0,
		"info":"error",
		"data":{
			"msg":"未查到此人"
		}
	}

## 八、消息接口
### 请求地址：
>消息列表接口
https://www.zzjsxns.top/api/msg/msglist
>列表接口说明:
*在有消息图标的页面请求此接口。lastId为最后一条消息的id,如果此id大于localStorage里的msgId则消息图标上显示红点提示，如果用户点击了消息，把localStorage里的msgId重置为lastId并取消红点提示。*

>未读消息数量接口
https://www.zzjsxns.top/api/msg/msgcount

>得到单条消息内容接口
https://www.zzjsxns.top/api/msg/getmsg

### 请求方式：GET/POST
### 请求参数：
>消息列表

| 参数名 | 参数类型 |是否必填 | 默认值 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| page | int | 否 | 1 | 页码 |
| rows | int | 否 | 10 | 每页条数 |

>未读消息数量

| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| msg_id | int | 是 | 10 | 已读的最后一条消息ID |

>单条消息

| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| id | int | 是 | 10 | 消息ID |

### 返回示例：
	消息列表返回示例
	{
		"total":2,
		"items":
		[
			{
				"id":2,
				"title":"会员卡过期通知",
				"create_at":"2021-07-19 15:44:16"
			},
			{
				"id":1,
				"title":"测试消息",
				"create_at":"2021-07-19 15:42:05"
			}
		],
		"totalPage":1,
		"lastId":2
	}
	未读消息返回示例
	3
	单条消息返回示例
	{
		"id":2,
		"title":"会员卡过期通知",
		"content":"<p>会员卡过期通知<\/p>\r\n\r\n<p>会员卡过期通知会员卡过期通知，会员卡过期通知会员卡过期通知，会员卡过期通知会员卡过期通知会员卡过期通知，<\/p>\r\n\r\n<p><strong><em>会员卡过期：<\/em><\/strong>通知会员卡过期通知，会员卡过期通知会员卡过期通知，会员卡过期通知会员卡过期通知会员卡过期通知，<\/p>",
		"create_at":"2021-07-19 15:44:16"
	}
	
## 九、告知接口
### 请求地址：
https://www.zzjsxns.top/api/common/getnotice
### 请求方式：GET/POST
### 请求参数：

| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| type | int | 是 | 10 | 消息类型 1:押金告知,2:违章告知,3:车况告知,4:保障告知,5:租车协议告知,6:出行隐私告知,7:一键登录协议告知 |

### 返回示例：
	消息列表返回示例
	{
		"id":8,
		"title":"押金告知",
		"content":"<p>第二个押金告知第二个押金告知第二个押金告知第二个押金告知第二个押金告知第二个押金告知<\/p>\r\n\r\n<p>第二个押金告知第二个押金告知第二个押金告知第二个押金告知第二个押金告知第二个押金告知<\/p>",
		"type":1,
		"create_at":"2021-07-27 16:48:46"
	}
	错误返回null

## 十、H5人脸验证
### 请求地址：
https://www.zzjsxns.top/api/face/getObj
### 请求方式：POST
>列表接口说明:
*err=0为成功，成功以后直接新开页面访问URL，无需其他操作*
### 请求参数：

| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| user_id | int | 是 | 10 | 当前用户ID |

### 返回示例：
	正确返回示例
	{
		"err":0,
		"verify_token":"tVtzxb8KvRUVcwrEX4dBv31x",
		"url":"https://brain.baidu.com/face/print/?token=tVtzxb8KvRUVcwrEX4dBv31x&successUrl=https%3A%2F%2Fwww.zzjsxns.top%2Fapi%2Fface%2Ffacesucc%3Fuser_id%3D4%26vt%3DtVtzxb8KvRUVcwrEX4dBv31x&failedUrl=https%3A%2F%2Fwww.zzjsxns.top%2Fapi%2Fface%2Ffacefail%3Fuser_id%3D4%26vt%3DtVtzxb8KvRUVcwrEX4dBv31x"
	}
	错误返回示例
	{
		"err":2, //0：成功(所有前置条件均已验证，人脸识别通过以后就能下单)，1：verify_token获取失败，2：身份信息上报失败，3：身份证未上传，4：用户ID不能为空，5：身份证号和驾驶证号不一致，6：请先上传驾照
		"verify_token":"",
		"msg":"身份信息上报失败"
	}

## 十一、获取小程序Openid和sessionKey
### 请求地址：
https://www.zzjsxns.top/api/openid/getInfo
### 请求方式：POST
| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| code | string | 是 | 100 | wx.login获取的code |
### 返回示例：
```json
正确返回示例
{"data":{"session_key":"tVtzxb%UVcwrEX4dBv31x","openid":"xxxxxx"}}
错误返回示例
{"errcode":41008,"errmsg":"missing code, rid: 6260bb3d-6bed6119-74945f5b"}
```
## 十二、获取小程序手机号并登录
### 请求地址：
https://www.zzjsxns.top/api/login/in
### 请求方式：POST
| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| code | string | 是 | 100 | getPhoneNumber获取的code |
### 返回示例：
```shell
正确返回示例
{
	code: 1
	data:{
		ai_type: 1
		auth_status: 1
		balance_gift: "0.00"
		balance_money: "0.00"
		bondsman: null
		bondsman_idcardno: ""
		bondsman_phone: null
		device_id: 0
		first_login_time: "2022-02-25 09:09:29"
		id: 39
		is_auto: 1
		is_first_order: 1
		last_license_order_time: "2022-03-14 11:43:54"
		license_id: 39
		need_assure: 0
		need_man_check: 0
		phone_no: "18201149012"
		photo: "https://speed.zzjsxns.top/file/510fcc6e0d6d5171b7960989c2ecbeb8/14182812980.png"
		register_time: "2022-02-25 09:09:29"
	}
	token:{
		expire: 1651121645
		token: "503dc36f355969c1f6a508838d9a2569"
	}
	total_km: "0.00"
	user_status: 0
	username: "李聪聪"
	uuid: "1645751369344578"
	info: "手机登录成功！"
}

错误返回示例
{
	code: 0
	data:{
		errcode: 47001
		errmsg: "data format error hint: [.gkBSbMre-CapunA] rid: 6260e4d9-488a3f43-5a2316d2"
	}
	info: "手机号解析失败"
}
```
## 十三、车辆列表/选车 接口
### 请求地址：
https://www.zzjsxns.top/api/car/search
### 请求方式：GET
| 参数名 | 参数类型 |是否必填 | 最大长度 | 参数说明 |
| :----:| :----:| :----:| :----: | :---- |
| is_index | int | 否 | 1 | 区分是首页数据还是选车页数据，is_index=1:首页，不传是选车页 |
| page_type | int | 否 | 1 | 如果选车页有筛选条件或排序条件则传1，否则不传，**首页一定不要传** |
| city_id | int | 否 | 100 | 定位到的用户所在城市ID |
| lng | float | 否 | 无 | 定位到的用户当前位置参数 |
| lat | float | 否 | 无 | 定位到的用户当前位置参数 |
| seats | int | 否 | 10 | 座位数 |
| model | string | 否 | 10 | 车型：值包括(轿车、SUV、跑车、MPV)，注意英文要大写 |
| gearbox | string | 否 | 10 | 变速箱：值包括(自动挡、手动挡) |
| brand_id | string | 否 | 2000 | 品牌ID：在选择车牌后选全部车系时传入。格式：18,20,23 |
| model_id | string | 否 | 2000 | 车系ID：在选择车牌后选某个车系时传入。格式：1,12,21 |
| price | string | 否 | 100 | 选择价格区间时传入，格式：500-1000 |
| price_sort | int | 否 | 1 | 按价格排序，1：价格从低到高，2：价格从高到低 |
| get_count | int | 否 | 1 | 值为1时返回筛选车辆的数量，值为0或不传时返回车辆列表 |

### 返回示例：
```json
//获取筛选车辆正确示例 get_count值为0或空
{
"code": 1,  // 1.请求成功，2.请求异常
"info": "success",  //接口返回标识  success:正常，error:异常
"data": 
	[
		{
		"id": 23,	//车辆ID
		"price": "0.01",	//现价
		"status": 1,	//车辆状态：1.空闲，2.在租
		"oprice": "1699.00",	//原价
		"name": "路虎揽胜  柴油",  	//车辆名称
		"image": "https://speed.zzjsxns.top/25/57924b143e38b5c4f410d8f46ee550.jpg",  //车辆图片
		"gearbox": "自动挡",  	//变速箱
		"drive": "全时四驱",	//驱动方式
		"seats": 5,		//座位数
		"displacement": "3.0", //排量
		"model": "SUV"	//车型
		},
		{
		"id": 7,
		"price": "0.01",
		"status": 1,
		"oprice": "2000.00",
		"name": "路虎揽胜运动",
		"image": "https://speed.zzjsxns.top/86/4a1756022c7bb1d3edf2b6ab479864.jpg",
		"gearbox": "自动挡",
		"drive": "四驱",
		"seats": 4,
		"displacement": "3.0",
		"model": "SUV"
		},
		{
		"id": 27,
		"price": "0.01",
		"status": 1,
		"oprice": "1699.00",
		"name": "路虎 揽胜",
		"image": "https://speed.zzjsxns.top/68/330a337f7b20824be26c110ae3d467.jpg",
		"gearbox": "8AT",
		"drive": "四驱",
		"seats": 5,
		"displacement": "3.0",
		"model": "SUV"
		},
		{
		"id": 30,
		"price": "0.01",
		"status": 1,
		"oprice": "1392.00",
		"name": "路虎揽胜行政加长",
		"image": "https://speed.zzjsxns.top/67/014961aad5aba1481b5ebe78555661.jpg",
		"gearbox": "自动挡",
		"drive": "四驱",
		"seats": 8,
		"displacement": "0.0",
		"model": "MPV"
		}
	]
}


//获取筛选车辆数量返回示例：get_count值为1
{
	"code": 1,
	"info": "success",
	"data": 4  //筛选出的数量
}

//错误示例
{
"code": 0,
"info": "价格区间错误",  //错误描述
"data": {}
}
```
## 十四、获得品牌加车系
### 请求地址：
https://www.zzjsxns.top/api/car/models
### 请求方式：GET 无参数
### 返回示例：
```json
{
"code": 1,
"info": "success",
"data": [
	{
	"id": 9, //品牌ID  (brand_id)
	"name": "玛莎拉蒂",  //品牌名称
	"image": "https://speed.zzjsxns.top/4a/3e2a364977141c67ea469e81aacf81.png", //品牌icon
	"models": [
		{
			"id": 21,  //车系ID（model_id）
			"bid": 9,
			"name": "总裁",  //车系名称
			"sort": 0,
			"deleted": 0,
			"create_at": "2022-06-06 15:59:49"
		},
		{
			"id": 22,
			"bid": 9,
			"name": "莱万特",
			"sort": 0,
			"deleted": 0,
			"create_at": "2022-06-06 16:00:27"
		},
		{
			"id": 23,
			"bid": 9,
			"name": "Gran Turismo",
			"sort": 0,
			"deleted": 0,
			"create_at": "2022-06-06 16:00:38"
		},
		{
			"id": 24,
			"bid": 9,
			"name": "吉博力",
			"sort": 0,
			"deleted": 0,
			"create_at": "2022-06-06 16:06:01"
		}
	]
	},
	{
	"id": 1,
	"name": "宝马",
	"image": "https://speed.zzjsxns.top/20/a8efef9d88bde1e4a916f21efd15de.png",
	"models": [
		{
			"id": 6,
			"bid": 1,
			"name": "三系",
			"sort": 1,
			"deleted": 0,
			"create_at": "2022-06-06 13:19:13"
		},
		{
			"id": 2,
			"bid": 1,
			"name": "X6",
			"sort": 0,
			"deleted": 0,
			"create_at": "2022-06-06 13:17:12"
		},
		{
			"id": 3,
			"bid": 1,
			"name": "五系",
			"sort": 0,
			"deleted": 0,
			"create_at": "2022-06-06 13:17:24"
		},
		{
			"id": 4,
			"bid": 1,
			"name": "七系",
			"sort": 0,
			"deleted": 0,
			"create_at": "2022-06-06 13:17:44"
		},
		{
			"id": 5,
			"bid": 1,
			"name": "X5",
			"sort": 0,
			"deleted": 0,
			"create_at": "2022-06-06 13:17:52"
		}
	]
	}
]
}
```